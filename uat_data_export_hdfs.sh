#!/bin/bash
#The purpose of this script is to fetch the daily data for all the tenants for all data types from E2-Live mongo database a                                             nd export to json files

on_error_exit ()
{
    if [ "$?" != "0" ]; then
/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell  -u "UAT E2deep data Loading for $l0_date2 Failed " -m " " 
  exit 1
    fi
}

cd /home/pgollakoti/scripts

ymd=$(date +"%Y/%m/%d" -d "0 day ago")
ymd2=$(date +%Y-%m-%d -d "0 day ago")
l0_date1=$(date --date="-1 day" +%Y-%m-%d )
l0_date2=$(date --date="-2 day" +%Y-%m-%d )
start_date='$gt:''"'$l0_date2'"'
end_date='$lt:''"'$l0_date1'"'

#query_value={"type":$j,"tenantId":$i,"createDate":{$start_date:l0_date2,$end_date:l0_date1}}
hostname="uatmongo1"


#mongo eagleeye_db  --host $hostname -u eagleeye_read -p 9864G6Kr9DE78Gn < getTenants.js > UAT_tenant_list.txt
#on_error_exit
#echo "Type retrieval started at:"`date`
#mongo eagleeye_db  --host uatmongo1 -u eagleeye_read  -p 9864G6Kr9DE78Gn  --quiet --eval "let start_date='"$l0_date2"',end_date='"$l0_date1"'" getTypes1.js > temp
#tail -1 temp > UAT_type_list.txt
#echo "Type retrieval completed  at:"`date`
#on_error_exit

k=0
while read p; do
k=$((k+1))
if [ $k -eq 6 ]; then
te=$p
fi
done < UAT_tenant_list.txt

k=0
while read p; do
k=$((k+1))
if [ $k -eq 1 ]; then
ty=$p
fi
done < UAT_type_list.txt

declare -a tenants=($te);
declare -a types=($ty);


for i in "${tenants[@]}"
do
tenant=`echo $i| tr -d '"'`
L0_dir="/nfspartition/UAT/tenants/"$tenant"/installation/"$ymd"/l0/"
L1_dir="/nfspartition/UAT/tenants/"$tenant"/installation/"$ymd"/l1_mongo/"
user_dir="/nfspartition/UAT/tenants/"$tenant"/installation/"$ymd"/user_data/"
user_output_dir="/nfspartition/UAT/tenants/"$tenant"/installation/"$ymd"/user_output/"
L0_monitoring_dir="/nfspartition/UAT/tenants/"$tenant"/monitoring/"$ymd"/l0/"

if [ ! -d "$L0_dir" ]; then
echo "creating directories"
mkdir -p $L0_dir
mkdir -p $L1_dir
mkdir -p $user_dir
mkdir -p $user_output_dir
mkdir -p $L0_monitoring_dir
fi


#query_value3={"tenantId":$i,"createDate":{$start_date,$end_date}}
query_value3={"tenantId":$i,"createDate":{$start_date}}
query_value2={"tenantId":$i}

#for j in "${types[@]}"
#do
#query_value={"type":$j,"tenantId":$i,"createDate":{$start_date}}
#echo $query_value
#data_type=`echo $j | tr -d '"'`"_data"".json"
#if [[ $j == +("sms"|"location") ]]; then
#mongoexport --host $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 9864G6Kr9DE78Gn  -c l0_monitoring  --query                                              "${query_value}"  -o $L0_monitoring_dir$data_type
#on_error_exit
#sed -i 's/$numberLong/numberLong/g' $L0_monitoring_dir$data_type
#fi

#mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 9864G6Kr9DE78Gn  -c user_l0  --query "${query_value}"  -o $L0_dir$data_type
#on_error_exit
#sed -i 's/$numberLong/numberLong/g' $L0_dir$data_type
#if [ $j == '"sms"' ]  && [ $i == '"1"' ]; then
#mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 9864G6Kr9DE78Gn  -c user_l1  --query "${query_value}"  -o $L1_dir$data_type
#on_error_exit
#sed -i 's/$numberLong/numberLong/g' $L1_dir$data_type
#fi
#done

user_out=$user_dir"user_data.json"
user_output_out=$user_output_dir"user_output.json"
mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 9864G6Kr9DE78Gn  -c sdk_users  --query "${query_value2}"  -o $user_out
on_error_exit
mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 9864G6Kr9DE78Gn  -c user_output  --query "${query_value3}"  -o $user_output_out
on_error_exit
sed -i 's/$numberLong/numberLong/g' "$user_output_out"

done


/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell -u "UAT E2deep data loading for $l0_date2 Completed Successfully " -m " "

