#!/bin/bash

on_error_exit ()
{
if [ "$?" != "0" ]; then
  /usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com,sankar.mittapally@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell  -u "Mongo Daily  backup failed" -a /incremental_backup/mongo_weekly/mongo_weekly.log
i   exit 1
fi
}


cd /database/incremental_backup/

host_name="prodmongohidden2"

timeStamp1=$(sed '4q;d' time_out.txt)
timeStamp2=$(sed '5q;d' time_out.txt)
query_value='{"ts":{$gt:Timestamp('"$timeStamp1","$timeStamp2"')}}'
ymd=$(date +"%Y-%m-%d_%H-%M")
rm -rf mongo_daily/*
echo "Dump started at:" `date`
mongodump --host $host_name  --authenticationDatabase admin -u admin -p OldM0nk123 --db local -c oplog.rs --query  "${query_value}" -o mongo_daily/$ymd
on_error_exit
echo "Dump completed at:" `date`
bsondump mongo_daily/$ymd/local/oplog.rs.bson > temp.json
on_error_exit
p=`awk '/./{line=$0} END{print line}' temp.json`
searchstring='"h"'
rest=${p#*$searchstring}
time_json=$(( ${#p} - ${#rest} - ${#searchstring}-1 ))
final=`echo $p | cut -b 1-$time_json`"}"
sed -i "s/time_val/$final/g" get_time.js
rm temp.json
> time_out.txt
mongo --host $host_name  --authenticationDatabase admin -u admin -p OldM0nk123 < get_time.js >> time_out.txt
on_error_exit
cp get_time2.js get_time.js

## compress and copy daily oplog dump to Amazon S3 and delete the files from server
echo "Compress and S3 upload started at:" `date`
cd /database/incremental_backup/mongo_daily
tar cvzf $ymd"_oplog_dump.tar.gz" $ymd
on_error_exit
/usr/local/bin/s3cmd put $ymd"_oplog_dump.tar.gz"  s3://creditvidya-mongobackups/E2Live/
on_error_exit
/usr/local/bin/s3cmd put /database/incremental_backup/time_out.txt s3://creditvidya-mongobackups/E2Live/
on_error_exit
echo "Compress and S3 upload completed at:" `date`
rm -rf *
/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com,sankar.mittapally@creditvidya.com,srikanth.gaddam@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell -u "Mongo Daily backup Successful" -m "Mongo Incremental Backup sucessful" -a /database/incremental_backup/mongo_daily.log  > /dev/null 2>&1
mv /database/incremental_backup/mongo_daily.log /database/incremental_backup/mongo_daily_$ymd.log

