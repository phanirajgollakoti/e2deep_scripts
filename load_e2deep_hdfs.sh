#This script loads the e2deep data from NFS to HDFS
#!/bin/bash
ymd=$(date +"%Y/%m/%d" -d "yesterday")
ym=$(date +"%Y/%m/" -d "yesterday")
k=0
while read p; do
k=$((k+1))
if [ $k -eq 6 ]; then
te=$p
fi
done < tenant_list.txt
tenants=($te);
for i in "${tenants[@]}"
do
tenant=`echo $i| tr -d '"'`
nfs_tenant_dir="/nfspartition/tenants/$tenant/installation/$ymd/"
hdfs_tenant_dir="/nfspartition/tenants/$tenant/installation/$ymd/"
hdfd dfs -mkdir -p $tenant_dir
hdfs dfs -put $nfs_tenant_dir $hdfs_tenant_dir
done
