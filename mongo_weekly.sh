
on_error_exit ()
{
if [ "$?" != "0" ]; then
  /usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com,sankar.mittapally@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell  -u "Mongo Weekly backup failed" -a /incremental_backup/mongo_weekly/mongo_weekly.log
   exit 1
fi
}

ymd=$(date +"%Y-%m-%d")
cd /database/incremental_backup/

host_name="prodmongohidden2"

mongodump --host $host_name --authenticationDatabase admin -u admin -p OldM0nk123 --oplog  -o mongo_weekly/$ymd
on_error_exit
cd mongo_weekly/$ymd
bsondump oplog.bson > temp.json
on_error_exit
p=`awk '/./{line=$0} END{print line}' temp.json`
searchstring='"h"'
rest=${p#*$searchstring}
time_json=$(( ${#p} - ${#rest} - ${#searchstring}-1 ))
final=`echo $p | cut -b 1-$time_json`"}"
echo $final
rm temp.json
cd /database/incremental_backup 
sed -i "s/time_val/$final/g" get_time.js
> time_out.txt
mongo admin  --host $host_name  --authenticationDatabase admin -u admin -p OldM0nk123 < get_time.js >> time_out.txt
on_error_exit
cp get_time2.js get_time.js


## compress and copy weekly full dump to Amazon S3 and delete the files from server 
cd /database/incremental_backup/mongo_weekly
tar cvzf $ymd"_full_dump.tar.gz" $ymd
on_error_exit
/usr/local/bin/s3cmd put $ymd"_full_dump.tar.gz"  s3://creditvidya-mongobackups/E2Live/
on_error_exit
/usr/local/bin/s3cmd put /database/incremental_backup/time_out.txt s3://creditvidya-mongobackups/E2Live/
on_error_exit
echo "Deleting Old backups from S3"
sh /database/incremental_backup/delete_s3.sh
/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com,srikanth.gaddam@creditvidya.com,sankar.mittapally@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell -u "Mongo Weekly  backup Successful"  -a /database/incremental_backup/mongo_weekly.log  > /dev/null 2>&1
rm -rf $ymd
mv  /database/incremental_backup/mongo_weekly.log /database/incremental_backup/mongo_weekly_$ymd.log


