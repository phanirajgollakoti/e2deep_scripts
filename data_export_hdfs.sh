#!/bin/bash
#The purpose of this script is to fetch the daily data for all the tenants for all data types from E2-Live mongo database a                                             nd export to json files

on_error_exit ()
{
    if [ "$?" != "0" ]; then
/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell  -u "E2deep data Loading for $l0_date2 Failed " -m " " -a /home/pgollakoti/scripts/e2deep_load.log
#mv /home/pgollakoti/scripts/e2deep_load.log /home/pgollakoti/scripts/e2deep_load_$ymd2.log
  exit 1
    fi
}

delete_date=$(date --date="-2 day" +%Y/%m/%d )
rm -rf /nfspartition/tenants/*/*/$delete_date
cd /home/pgollakoti/scripts

ymd=$(date +"%Y/%m/%d" -d "1 days ago")
ymd2=$(date +%Y-%m-%d -d "1 days ago")
l0_date1=$(date --date="-0 day" +%Y-%m-%d )
l0_date2=$(date --date="-1 day" +%Y-%m-%d )
start_date='$gt:''"'$l0_date2'"'
end_date='$lt:''"'$l0_date1'"'

#query_value={"type":$j,"tenantId":$i,"createDate":{$start_date:l0_date2,$end_date:l0_date1}}
hostname="prodmongohidden2"


mongo eagleeye_db  --host $hostname -u eagleeye_read -p 5PZ4M4W7yZeeJuM < getTenants.js > tenant_list1.txt
on_error_exit
echo "Type retrieval started at:"`date`
mongo eagleeye_db  --host prodmongohidden2 -u eagleeye_read  -p 5PZ4M4W7yZeeJuM  --quiet --eval "let start_date='"$l0_date2"',end_date='"$l0_date1"'" getTypes1.js > temp
tail -1 temp > type_list.txt
echo "Type retrieval completed  at:"`date`
on_error_exit

cp tenant_list1.txt /nfspartition/


k=0
while read p; do
k=$((k+1))
if [ $k -eq 6 ]; then
te=$p
fi
done < tenant_list1.txt

#te=$( tail -n 1 tenant_list.txt )

k=0
while read p; do
k=$((k+1))
if [ $k -eq 1 ]; then
ty=$p
fi
done < type_list.txt

declare -a tenants=($te);
declare -a types=($ty);


for i in "${tenants[@]}"
do
tenant=`echo $i| tr -d '"'`
L0_dir="/nfspartition/tenants/"$tenant"/installation/"$ymd"/l0/"
L1_dir="/nfspartition/tenants/"$tenant"/installation/"$ymd"/l1_mongo/"
user_dir="/nfspartition/tenants/"$tenant"/installation/"$ymd"/user_data/"
user_output_dir="/nfspartition/tenants/"$tenant"/installation/"$ymd"/user_output/"
L0_monitoring_dir="/nfspartition/tenants/"$tenant"/monitoring/"$ymd"/l0/"

if [ ! -d "$L0_dir" ]; then
echo "creating directories"
mkdir -p $L0_dir
mkdir -p $L1_dir
mkdir -p $user_dir
mkdir -p $user_output_dir
mkdir -p $L0_monitoring_dir
fi


query_value3={"tenantId":$i,"createDate":{$start_date,$end_date}}
query_value2={"tenantId":$i}

for j in "${types[@]}"
do
query_value={"type":$j,"tenantId":$i,"createDate":{$start_date,$end_date}}
echo $query_value
data_type=`echo $j | tr -d '"'`"_data"".json"
if [ $j == '"sms"' ] || [ $j == '"location"' ]; then
mongoexport --host $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 5PZ4M4W7yZeeJuM  -c l0_monitoring  --query                                              "${query_value}"  -o $L0_monitoring_dir$data_type
on_error_exit
sed -i 's/$numberLong/numberLong/g' $L0_monitoring_dir$data_type
fi

mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 5PZ4M4W7yZeeJuM  -c user_l0  --query "${query_value}"  -o $L0_dir$data_type
on_error_exit
sed -i 's/$numberLong/numberLong/g' $L0_dir$data_type

if [ $j == '"device"' ]; then
grep '"memoryInfo":\[' $L0_dir$data_type > $L0_dir"device_data_v1.json"
sed -i '/"memoryInfo":\[/d' $L0_dir$data_type
fi

if [ $j == '"sms"' ]  && [ $i == '"1"' ]; then
mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 5PZ4M4W7yZeeJuM  -c user_l1  --query "${query_value}"  -o $L1_dir$data_type
on_error_exit
sed -i 's/$numberLong/numberLong/g' $L1_dir$data_type
fi
done

user_out=$user_dir"user_data.json"
user_output_out=$user_output_dir"user_output.json"
mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 5PZ4M4W7yZeeJuM  -c sdk_users  --query "${query_value2}"  -o $user_out
on_error_exit


query_value4={"tenantId":$i,"createDate":/$ymd2/}
echo $query_value4
user_add_out=$user_dir"user_additional_info.json"
mongoexport --host prodmongohidden2 --db eagleeye_db -u pgollakoti -p vpx6kKWqRa5UQD2u  -c user_additional_info --query "${query_value4}"  -o $user_add_out
sed -i 's/$numberLong/numberLong/g'  $user_add_out

mongoexport --host  $hostname  --port 27017 --db eagleeye_db  -u eagleeye_read -p 5PZ4M4W7yZeeJuM  -c user_output  --query "${query_value3}"  -o $user_output_out
on_error_exit
sed -i 's/$numberLong/numberLong/g' "$user_output_out"

done


/usr/local/bin/sendEmail -o tls=yes -f operations@creditvidya.com -t phaniraj.gollakoti@creditvidya.com -s smtp.gmail.com:587 -xu operations@creditvidya.com -xp alliswell -u "E2deep data loading for $l0_date2 Completed Successfully " -m " "

touch /nfspartition/status.txt
chmod 777 /nfspartition/status.txt
mv /home/pgollakoti/scripts/e2deep_load.log /home/pgollakoti/scripts/e2deep_load_$ymd2.log
